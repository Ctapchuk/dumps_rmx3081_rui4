#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:7bfb636733a21c665aa2fb065caf1fdd6e94f354; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:f1ca1d1b8c57375795fd5244cc6353741bd4c9be \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:7bfb636733a21c665aa2fb065caf1fdd6e94f354 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
